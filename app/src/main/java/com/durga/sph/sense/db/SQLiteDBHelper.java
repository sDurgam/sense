package com.durga.sph.sense.db;

/**
 * Created by durga on 7/16/15.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteDBHelper extends SQLiteOpenHelper
{
    public SQLiteDBHelper(Context context, String name, CursorFactory factory,
                          int version)
    {
        super(context, name, factory, version);
    }

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "ctx_sense.db";

    // User table name
    public static final String TABLE_WIDGETS = "ctx_widgets";

    //User columns
    public static final String KEY_ID = "id";
    public static final String KEY_LEFT = "left";
    public static final String KEY_TOP = "top";
    public static final String KEY_HEIGHT = "height";
    public static final String KEY_WIDTH = "width";

    String CREATE_WIDGETS_TABLE = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY NOT NULL, %s INTEGER DEFAULT 0, %s INTEGER DEFAULT 0, %s INTEGER DEFAULT 0, %s INTEGER DEFAULT 0)", TABLE_WIDGETS, KEY_ID, KEY_LEFT, KEY_TOP, KEY_HEIGHT, KEY_WIDTH);

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WIDGETS);
        db.execSQL(CREATE_WIDGETS_TABLE);
    }

    public SQLiteDBHelper (Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onCreate(db);
    }
}
