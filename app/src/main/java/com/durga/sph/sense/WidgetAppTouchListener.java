//package com.durga.sph.sense;
//
//import android.view.MotionEvent;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//
//
///**
// * Created by durga on 7/15/15.
// */
//public class WidgetAppTouchListener implements View.OnTouchListener
//{
//    int leftmargin;
//    int topmargin;
//    int rootwidth;
//    int rootheight;
//    SWidget widget;
//    SWidgetsMapper widgetsmapper;
//
//    public WidgetAppTouchListener(SWidget wid, SWidgetsMapper mapper)
//    {
//        widget = wid;
//        widgetsmapper = mapper;
//    }
//
//    @Override
//    public boolean onTouch(View v, MotionEvent event)
//    {
//        switch(event.getAction())
//        {
//            case MotionEvent.ACTION_MOVE:
//                leftmargin=(int)event.getRawX()-v.getWidth()/2;
//                topmargin=(int)event.getRawY()-v.getHeight()/2;
//                rootwidth=v.getRootView().getWidth();
//                rootheight=v.getRootView().getHeight();
//                RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(v.getWidth(),v.getHeight());
//                if(leftmargin+v.getWidth()>rootwidth)
//                    leftmargin=rootwidth-v.getWidth();
//                else if(leftmargin<0)
//                    leftmargin=0;
//                if(topmargin+v.getHeight()>((View)v.getParent()).getHeight())
//                    topmargin=((View)v.getParent()).getHeight()-v.getHeight();
//                else if(topmargin<0)
//                    topmargin=0;
//                lp.leftMargin=leftmargin;
//                lp.topMargin=topmargin;
//                v.setLayoutParams(lp);
//                //update new position of the widget
//                widget.setLeft(leftmargin);
//                widget.setTop(topmargin);
//                widgetsmapper.UpdateWidgets(widget);
//                break;
//            case MotionEvent.ACTION_UP:
//                v.setOnTouchListener(null);
//                //((LinearLayout)v.getParent().getParent()).setVisibility(View.INVISIBLE);
//                ((LinearLayout)((View)v).findViewById(R.id.mergeWidgetLayout)).setVisibility(View.INVISIBLE);
//                widgetsmapper.UpdateWidgets(widget);
//                //((LauncherAppWidgetHost)v.getTag()).startListening();
//                break;
//        }
//        return true;
//    }
//}
