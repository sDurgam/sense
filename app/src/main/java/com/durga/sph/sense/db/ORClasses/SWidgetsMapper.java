package com.durga.sph.sense.db.ORClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.durga.sph.sense.SWidget;
import com.durga.sph.sense.db.SQLiteDBHelper;

import java.util.ArrayList;

/**
 * Created by durga on 7/16/15.
 */
public class SWidgetsMapper extends BaseMapper
{
    int id;
    int left;
    int top;
    int height;
    int width;
    long result;

    public SWidgetsMapper(SQLiteDBHelper helper)
    {
        super(helper);
    }

    public SWidget GetWidget(int id)
    {
        SWidget swidget = new SWidget();
        SQLiteDatabase reader = dbHelper.getReadableDatabase();
        String[] columns = {SQLiteDBHelper.KEY_ID, SQLiteDBHelper.KEY_LEFT, SQLiteDBHelper.KEY_TOP, SQLiteDBHelper.KEY_HEIGHT, SQLiteDBHelper.KEY_WIDTH};
        String whereClause = SQLiteDBHelper.KEY_ID + "= ?";
        Cursor getWidgetCursor = reader.query(SQLiteDBHelper.TABLE_WIDGETS, columns, whereClause, new String[]{String.valueOf(id)}, null, null, null, null);
        getWidgetCursor.moveToFirst();
        if(getWidgetCursor.getCount() == 1)
        {
            swidget.setId(getWidgetCursor.getInt(0));
            swidget.setLeft(getWidgetCursor.getInt(1));
            swidget.setTop(getWidgetCursor.getInt(2));
            swidget.setHeight(getWidgetCursor.getInt(3));
            swidget.setWidth(getWidgetCursor.getInt(4));
        }
        getWidgetCursor.close();
        reader.close();;
        return swidget;
    }


    public ArrayList<SWidget> GetAllWidgets()
    {
        ArrayList<SWidget> widgetsList = new ArrayList<SWidget>();
        SWidget swidget;
        SQLiteDatabase reader = dbHelper.getReadableDatabase();
        String[] columns = {SQLiteDBHelper.KEY_ID, SQLiteDBHelper.KEY_LEFT, SQLiteDBHelper.KEY_TOP, SQLiteDBHelper.KEY_HEIGHT, SQLiteDBHelper.KEY_WIDTH};
        String whereClause = SQLiteDBHelper.KEY_ID + "= ?";
        //		Cursor cursor = reader.query(SQLiteDBHelper.TABLE_COMPANY, new String[] { SQLiteDBHelper.COMPANY_NAME, SQLiteDBHelper.COMPANY_URL, SQLiteDBHelper.COMPANY_MYRCARD_SENT}, );
        Cursor getWidgetCursor = reader.query(SQLiteDBHelper.TABLE_WIDGETS, columns, null, null, null, null, null, null);
        getWidgetCursor.moveToFirst();
        if(getWidgetCursor.getCount() > 0)
        {
            do {
                swidget = new SWidget();
                swidget.setId(getWidgetCursor.getInt(0));
                swidget.setLeft(getWidgetCursor.getInt(1));
                swidget.setTop(getWidgetCursor.getInt(2));
                swidget.setHeight(getWidgetCursor.getInt(3));
                swidget.setWidth(getWidgetCursor.getInt(4));
                widgetsList.add(swidget);
            }while(getWidgetCursor.moveToNext());
        }
        getWidgetCursor.close();
        reader.close();;
        return widgetsList;
    }

    public void InsertWidget(SWidget swidget)
    {
        SQLiteDatabase writer = dbHelper.getWritableDatabase();
        //int id = GetWidget(writer, swidget.getId());
        ContentValues cv = new ContentValues();
        cv.put(SQLiteDBHelper.KEY_ID, swidget.getId());
        cv.put(SQLiteDBHelper.KEY_LEFT, swidget.getLeft());
        cv.put(SQLiteDBHelper.KEY_TOP, swidget.getTop());
        cv.put(SQLiteDBHelper.KEY_HEIGHT, swidget.getHeight());
        cv.put(SQLiteDBHelper.KEY_WIDTH, swidget.getWidth());
        result = writer.insertWithOnConflict(SQLiteDBHelper.TABLE_WIDGETS, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        writer.close();
    }

    public void UpdateWidgets(SWidget swidget)
    {
        SQLiteDatabase writer = dbHelper.getWritableDatabase();
        String whereClause = SQLiteDBHelper.KEY_ID + "= ?";
        String[] whereArgs = new String[] { String.valueOf(swidget.getId())};
        ContentValues cv = new ContentValues();
        cv.put(SQLiteDBHelper.KEY_ID, swidget.getId());
        cv.put(SQLiteDBHelper.KEY_LEFT, swidget.getLeft());
        cv.put(SQLiteDBHelper.KEY_TOP, swidget.getTop());
        cv.put(SQLiteDBHelper.KEY_HEIGHT, swidget.getHeight());
        cv.put(SQLiteDBHelper.KEY_WIDTH, swidget.getWidth());
        result = writer.update(SQLiteDBHelper.TABLE_WIDGETS, cv, whereClause, whereArgs);
        writer.close();
    }

    public void DeleteWidget(int widgetId)
    {
        SQLiteDatabase writer = dbHelper.getWritableDatabase();
        String whereClause = SQLiteDBHelper.KEY_ID + "= ?";
        String[] whereArgs = new String[] { String.valueOf(widgetId)};
        result = writer.delete(SQLiteDBHelper.TABLE_WIDGETS, whereClause, whereArgs);
        writer.close();
    }
}
