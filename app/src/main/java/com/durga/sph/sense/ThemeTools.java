package com.durga.sph.sense;

/**
 * Created by durga on 6/30/15.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.util.List;

public class ThemeTools {

    public pac[] getAllThemes(PackageManager pm) {
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory("com.anddoes.launcher.THEME");
        List<ResolveInfo> packs = pm.queryIntentActivities( mainIntent, 0);
        pac[] pacs = new pac[packs.size()];
        for(int I=0;I<packs.size();I++) {
            pacs[I]= new pac();
            pacs[I].image=packs.get(I).loadIcon(pm);
            pacs[I].packagename= packs.get(I).activityInfo.packageName;
            pacs[I].name=packs.get(I).activityInfo.name;
            pacs[I].label=packs.get(I).loadLabel(pm).toString();
        }
        return pacs;
    }

    public static float getScaleFactor(Resources res,String string){
        float scaleFactor=1.0f;
        XmlResourceParser xrp = null;
        XmlPullParser xpp=null;
        try{
            int n;
            if ((n = res.getIdentifier("appfilter", "xml", string)) != 0) {
                xrp = res.getXml(n);
                System.out.println(n);
            } else {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setValidating(false);
                xpp = factory.newPullParser();
                InputStream raw = res.getAssets().open("appfilter.xml");
                xpp.setInput(raw, null);
            }

            if (n!=0){
                while (xrp.getEventType() != XmlResourceParser.END_DOCUMENT && scaleFactor==1.0f) {
                    if (xrp.getEventType()==2){
                        try{
                            String s = xrp.getName();
                            if (s.equals("scale")) {
                                scaleFactor = Float.parseFloat(xrp.getAttributeValue(0));
                            }
                        }catch(Exception e){}
                    }
                    xrp.next();
                }
            }
            else{
                while (xpp.getEventType() != XmlPullParser.END_DOCUMENT && scaleFactor==1.0f) {
                    if (xpp.getEventType()==2){
                        try{
                            String s = xpp.getName();
                            if (s.equals("scale")) {
                                scaleFactor = Float.parseFloat(xpp.getAttributeValue(0));
                            }
                        }catch(Exception e){}
                    }
                    xpp.next();
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return scaleFactor;
    }


    public static String getResourceName(Resources res,String string,String componentInfo){
        String resource = null;
        XmlResourceParser xrp = null;
        XmlPullParser xpp=null;
        try{
            int n;
            if ((n = res.getIdentifier("appfilter", "xml", string)) != 0) {
                xrp = res.getXml(n);
            } else {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setValidating(false);
                xpp = factory.newPullParser();
                InputStream raw = res.getAssets().open("appfilter.xml");
                xpp.setInput(raw, null);
            }

            if (n!=0){
                while (xrp.getEventType() != XmlResourceParser.END_DOCUMENT && resource==null) {
                    if (xrp.getEventType()==2){
                        try{
                            String s = xrp.getName();
                            if (s.equals("item")) {
                                if (xrp.getAttributeValue(0).compareTo(componentInfo)==0){
                                    resource = xrp.getAttributeValue(1);
                                }
                            }
                        }catch(Exception e){}
                    }
                    xrp.next();
                }
            }
            else{
                while (xpp.getEventType() != XmlPullParser.END_DOCUMENT && resource==null) {
                    if (xpp.getEventType()==2){
                        try{
                            String s = xpp.getName();
                            if (s.equals("item")) {
                                if (xpp.getAttributeValue(0).compareTo(componentInfo)==0){
                                    resource = xpp.getAttributeValue(1);
                                }
                            }
                        }catch(Exception e){}
                    }
                    xpp.next();
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return resource;
    }


    public static String [] getIconBackAndMaskResourceName(Resources res,String packageName){
        String[] resource = new String[3];
        XmlResourceParser xrp = null;
        XmlPullParser xpp=null;
        try{
            int n;
            if ((n = res.getIdentifier("appfilter", "xml", packageName)) != 0) {
                xrp = res.getXml(n);
            } else {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setValidating(false);
                xpp = factory.newPullParser();
                InputStream raw = res.getAssets().open("appfilter.xml");
                xpp.setInput(raw, null);
            }

            if (n!=0){
                while (xrp.getEventType() != XmlResourceParser.END_DOCUMENT && (resource[0]==null || resource[1]==null || resource[2]==null)) {
                    if (xrp.getEventType()==2){
                        try{
                            String s = xrp.getName();
                            if (s.equals("iconback")) {
                                resource[0] = xrp.getAttributeValue(0);
                            }
                            if (s.equals("iconmask")) {
                                resource[1] = xrp.getAttributeValue(0);
                            }
                            if (s.equals("iconupon")) {
                                resource[2] = xrp.getAttributeValue(0);
                            }
                        }catch(Exception e){}
                    }
                    xrp.next();
                }
            }
            else{
                while (xpp.getEventType() != XmlPullParser.END_DOCUMENT && (resource[0]==null || resource[1]==null || resource[2]==null)) {
                    if (xpp.getEventType()==2){
                        try{
                            String s = xpp.getName();
                            if (s.equals("iconback")) {
                                resource[0] = xpp.getAttributeValue(0);
                            }
                            if (s.equals("iconmask")) {
                                resource[1] = xpp.getAttributeValue(0);
                            }
                            if (s.equals("iconupon")) {
                                resource[2] = xpp.getAttributeValue(0);
                            }
                        }catch(Exception e){}
                    }
                    xpp.next();
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return resource;
    }

    public static int numtodp(int in, Activity activity){
        int out = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, in, activity.getResources().getDisplayMetrics());
        return out;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        return resizedBitmap;
    }

    public static Matrix getResizedMatrix(Bitmap bm, int newHeight, int newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return matrix;
    }

    public static Bitmap drawableToBitmap(Drawable drawable)
    {
        if(drawable instanceof BitmapDrawable)
        {
            return ((BitmapDrawable)drawable).getBitmap();
        }
        else
        {
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas();
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        }
    }

}
