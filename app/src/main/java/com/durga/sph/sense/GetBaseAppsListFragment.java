package com.durga.sph.sense;

import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.trnql.smart.base.SmartCompatFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 * Created by durga on 7/9/15.
 */
public class GetBaseAppsListFragment extends SmartCompatFragment
{
    PackageManager packageManager;
    List<ResolveInfo> pacsList;
    pac[] pacs;
    //Layout attributes
    RecyclerView appsView;
    SAppAdapter appAdapterObj;


    @Override
    public void onResume() {
        super.onResume();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this.getActivity(),1);
        gridLayoutManager.setSmoothScrollbarEnabled(true);
        appsView.setLayoutManager(gridLayoutManager);
        appsView.setHasFixedSize(true);
        gridLayoutManager.setSmoothScrollbarEnabled(true);
        appsView.setHorizontalScrollBarEnabled(true);
        appsView.setVerticalScrollBarEnabled(false);
    }

    protected void DisplayInstalledAppsList(boolean init)
    {
        pac app;
        ResolveInfo info;
        String name;
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        pacsList = packageManager.queryIntentActivities(mainIntent, 0);
        pacs = new pac[pacsList.size()];
        ArrayList<pac> installedappNamesList = new ArrayList<pac>();
        StoresListing.storesMap = new HashMap<String, String>();
        for(int i =0; i < pacsList.size(); i++)
        {
            info = pacsList.get(i);
            app = new pac();
            if(packageManager != null && info.loadLabel(packageManager) != null)
            {
                pacs[i] = new pac();
                pacs[i].setName(info.activityInfo.name);
                pacs[i].setPackagename(info.activityInfo.packageName);
                pacs[i].setImage(info.loadIcon(packageManager));
                pacs[i].setLabel(info.loadLabel(packageManager).toString());
            }
        }
        new SortApps().exchange_sort(pacs);
        //themePacs();
        //set the recyclerview to display all the installed app names
        if(init)
        {
            //get default icon height and width
            Drawable drawable = getResources().getDrawable(R.drawable.ic_launcher);
            appAdapterObj = new SAppAdapter(this.getActivity(), pacs, packageManager, drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth());
            appsView.setAdapter(appAdapterObj);
        }
        else
        {
            appAdapterObj.appsList = pacs;
            appAdapterObj.notifyDataSetChanged();
        }
    }


    protected void DisplayTopAppsList(boolean init)
    {
        int nullintents = 0;
        String nullitems = "";
        pac app;
        //ResolveInfo info;
        //ApplicationInfo info;
        String name;
        Intent intent = new Intent();
        ActivityInfo info;
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<pac> pacsLIst = new ArrayList<pac>();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            UsageStatsManager usagemanager = (UsageStatsManager) this.getActivity().getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List<UsageStats> stats = usagemanager.queryUsageStats(UsageStatsManager.INTERVAL_BEST, time - 1000 * 10000, time);
            //stats.get(0).getClass().
            if (stats != null) {
                SortedMap<Long, String> topAppsMap = new TreeMap<Long, String>();
                for (UsageStats st : stats)
                {
                    topAppsMap.put(st.getTotalTimeInForeground(), st.getPackageName());
                }
                if (topAppsMap != null) {
                    //int i = Math.min(20, topAppsMap.size());
                    int max = topAppsMap.size();
                    int i = 0;
                    try {
                        for (Map.Entry<Long, String> entry : topAppsMap.entrySet()) {
                            app = new pac();

                            intent = packageManager.getLaunchIntentForPackage(entry.getValue());
                            if(intent == null)
                            {
                                nullintents ++;
                                nullitems += "" + entry.getValue();
                            }
                            else
                            {
                                info = packageManager.getActivityInfo(intent.getComponent(), 0);
                                if (info.loadLabel(packageManager) != null)
                                {
                                    app.setName(info.name);
                                    app.setPackagename(info.packageName);
                                    app.setImage(info.loadIcon(packageManager));
                                    app.setLabel(info.loadLabel(packageManager).toString());
                                    pacsLIst.add(app);
                                }
                            }
                        }
                    } catch (Exception ex)
                    {
                        Log.e("Sense top apps", i + ex.getStackTrace().toString());
                    }
                }
            }
        }
        else {
//            ActivityManager activityManager = (ActivityManager) this.getActivity().getSystemService(Context.ACTIVITY_SERVICE);
//            List<ActivityManager.RecentTaskInfo> recentTaskInfos = activityManager.getRecentTasks(10, 0);
//            List<PackageInfo> pckInfoList;
//            for(PackageInfo pinfo : pckInfoList) {
//
//                intent = packageManager.getLaunchIntentForPackage(pinfo.packageName);
//                if (intent == null) {
//                    nullintents++;
//                    nullitems += "" + pinfo.packageName;
//                } else {
//                    try {
//                        info = packageManager.getActivityInfo(intent.getComponent(), 0);
//                        app = new pac();
//                        if (info.loadLabel(packageManager) != null)
//                        {
//                            app.setName(info.name);
//                            app.setPackagename(info.packageName);
//                            app.setImage(info.loadIcon(packageManager));
//                            app.setLabel(info.loadLabel(packageManager).toString());
//                            pacsLIst.add(app);
//                        }
//                    }
//                    catch (Exception ex)
//                    {
//                        Log.e("Sense top apps", ex.getStackTrace().toString());
//                    }
        }
        Collections.reverse(pacsLIst);
        pacs = new pac[pacsLIst.size()];
        pacsLIst.toArray(pacs);
        if(init)
        {
            //get default icon height and width
            Drawable drawable = getResources().getDrawable(R.drawable.ic_launcher);
            appAdapterObj = new SAppAdapter(this.getActivity(), pacs, packageManager, drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth());
            appsView.setAdapter(appAdapterObj);
        }
        else
        {
            appAdapterObj.appsList = pacs;
            appAdapterObj.notifyDataSetChanged();
        }
    }
}
