package com.durga.sph.sense;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by durga on 7/9/15.
 */
public class TopAppsFragment extends GetBaseAppsListFragment
{
    SAppAdapter appAdapterObj;
    static Activity activity;
    static boolean appLauncheable;
    AppReceiver appreceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        View allAppsView =  inflater.inflate(R.layout.fragment_topapps, container, false);
        appsView = (RecyclerView) allAppsView.findViewById(R.id.topAppsView);
        packageManager = this.getActivity().getPackageManager();
        activity = this.getActivity();
        return  allAppsView;
    }


    @Override
    public void onResume()
    {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addDataScheme("package");
        appreceiver = new AppReceiver();
        activity.registerReceiver(appreceiver, filter);
        DisplayTopAppsonScreen();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        activity.unregisterReceiver(appreceiver);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    void DisplayTopAppsonScreen()
    {
        appLauncheable = true;
        DisplayTopAppsList(true);
    }


    public class AppReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            DisplayTopAppsList(false);
        }
    }

}
