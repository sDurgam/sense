package com.durga.sph.sense;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by durga on 6/26/15.
 */


public class SAppAdapter extends RecyclerView.Adapter<SAppAdapter.AppViewHolder>
{
    Context ctx;
    pac[] appsList;
    PackageManager pmListener;
    LayoutInflater inflater;
    int height;
    int width;

    public SAppAdapter(Context context, pac[] appslist, PackageManager pm, int h, int w)
    {
        super();
        ctx = context;
        appsList = appslist;
        pmListener = pm;
        inflater =  LayoutInflater.from(ctx);
        height = h;
        width = w;
    }

   public static class AppViewHolder extends  RecyclerView.ViewHolder
    {
        //CardView cardView;
        public TextView name;
        public ImageView icon;
        public String packagename;
        public AppViewHolder(View view, int height, int width)
        {
            super(view);
            name = (TextView) view.findViewById(R.id.sname);
            icon = (ImageView) view.findViewById(R.id.sicon);
            icon.getLayoutParams().height = height;
            icon.getLayoutParams().width = width;
            //cardView = (CardView) view.findViewById(R.id.appcard);

        }
        public ImageView getIcon()
        {
            return icon;
        }

        public void setIcon(ImageView icon)
        {
            this.icon = icon;
        }

        public TextView getName()
        {
            return name;
        }

        public void setName(TextView name)
        {
            this.name = name;
        }


    }


    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflater.inflate(R.layout.appinfo_row_grid, parent, false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String[] attrs = (String[]) (((TextView)v.findViewById(R.id.sname)).getTag());
                int position = (int) (((TextView)v.findViewById(R.id.sname)).getTag());
                pac App = appsList[position];
                String[] attrs = new String[] { App.name, App.packagename};
                if(InstalledAppsFragment.appLauncheable) {
                    //Intent intent = pmListener.getLaunchIntentForPackage(attrs[1]);
                    Intent launchIntent = new Intent(Intent.ACTION_MAIN);
                    launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                    ComponentName cp = new ComponentName(attrs[1], attrs[0]);
                    launchIntent.setComponent(cp);
                    ctx.startActivity(launchIntent);
                }
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v)
            {
                InstalledAppsFragment.appLauncheable = false;
                RelativeLayout.LayoutParams lp = new  RelativeLayout.LayoutParams(v.getWidth(), v.getHeight());
                lp.leftMargin = (int) view.getX();
                lp.topMargin = (int) view.getY();
                LayoutInflater li = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout ll = (LinearLayout) li.inflate(R.layout.appinfo_row_grid, null);
                ((ImageView)ll.findViewById(R.id.sicon)).setImageDrawable(((ImageView)view.findViewById(R.id.sicon)).getDrawable());
                ((TextView)ll.findViewById(R.id.sname)).setText(((TextView)view.findViewById(R.id.sname)).getText());

                ll.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        v.setOnTouchListener(new AppTouchListener());
                        return false;
                    }
                });
                //String[] attrs = (String[]) (((TextView)v.findViewById(R.id.sname)).getTag());
                int position = (int) (((TextView)v.findViewById(R.id.sname)).getTag());
                pac App = appsList[position];
                String[] attrs = new String[] { App.name, App.packagename};
                ll.setOnClickListener(new AppClickListener(ctx, attrs));
                AppSerializableData objectData = SerializationTools.LoadSerializableData();
                if(objectData == null)
                {
                    objectData = new AppSerializableData();
                }
                if(objectData.apps == null)
                {
                    objectData.apps = new ArrayList<pac>();
                }
                pac toAdd = App;
                toAdd.X = lp.leftMargin;
                toAdd.Y = lp.topMargin;
                if(InstalledAppsFragment.activity.getResources().getConfiguration().orientation  == Configuration.ORIENTATION_LANDSCAPE)
                    toAdd.landscape = true;
                else
                    toAdd.landscape = false;
                toAdd.CacheIcon();
                objectData.apps.add(toAdd);
                SerializationTools.SerializableData(objectData);
                return false;
            }
        });
        return  new AppViewHolder(view, height, width);

    }

    @Override
    public void onBindViewHolder(AppViewHolder holder, int position) {
        pac app = appsList[position];
        //holder.setIsRecyclable((position%2 == 0));
        if(app.label == null)
        {
            holder.name.setText("no label");
        }
        holder.name.setText(app.label);
        int attrs = position;
        holder.name.setTag(attrs);
        holder.icon.setImageDrawable(app.image);

    }

    @Override
    public int getItemCount()
    {
        return appsList.length;
    }
}
