//package com.durga.sph.sense;
//
//import android.content.Context;
//import android.content.pm.ApplicationInfo;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.content.pm.ResolveInfo;
//import android.os.AsyncTask;
//import android.os.Handler;
//import android.os.Message;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by durga on 6/26/15.
// */
//public class LoadApplications extends AsyncTask<Void, Void, List<ResolveInfo>>
//{
//    Context lctx;
//    Handler lhandler;
//    PackageManager lpackageManager;
//
//    public LoadApplications(Context ctx, Handler handler, PackageManager packageManager)
//    {
//        super();
//        lctx = ctx;
//        lhandler = handler;
//        lpackageManager = packageManager;
//    }
//
//    @Override
//    protected List<ResolveInfo> doInBackground(Void... params)
//    {
//        return lpackageManager.queryIntentActivities();
//        //return lpackageManager.getInstalledPackages(0);
//    }
//
//    @Override
//    protected void onPostExecute(List<ApplicationInfo> result)
//    {
//        super.onPostExecute(result);
//        Message msg = new Message();
//        msg.what = 1;
//        msg.obj = result;
//        lhandler.sendMessage(msg);
//    }
//
//    @Override
//    protected void onPreExecute()
//    {
//        super.onPreExecute();
//    }
//
//    @Override
//    protected void onProgressUpdate(Void... values)
//    {
//        super.onProgressUpdate(values);
//    }
//}
