package com.durga.sph.sense;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.durga.sph.sense.db.ORClasses.SWidgetsMapper;
import com.durga.sph.sense.db.SQLiteDBHelper;

import java.util.ArrayList;


/**
 * Created by durga on 7/9/15.
 */
public class WidgetsFragment extends Fragment
{
    //App widgets
    SQLiteDBHelper dbHelper;
    SWidgetsMapper widgetsmapper;
    AppWidgetManager widgetmanager;
    LauncherAppWidgetHost widgethost;
    int REQUEST_CREATE_APPWIDGET = 10;
    int REQUEST_PICK_WIDGET = 11;
    int REQUEST_PICK_SHORTCUT = 12;
    int REQUEST_CREATE_SHORTCUT = 13;
    int REQUEST_PICK_THEME = 14;
    int REQUEST_CREATE_THEME = 15;
    int numwidgets;
    RelativeLayout widgetsLayout;
    Button addwidgetBtn;
    Context ctx;
    LayoutInflater minflater;

    @Override
    public void onStart()
    {
        super.onStart();
        widgethost.startListening();

    }

    @Override
    public void onStop()
    {
        widgethost.stopListening();
        super.onStop();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        widgetsLayout.requestFocus();
        if(widgetsLayout.getChildCount() == 0)
        {
            dbHelper = new SQLiteDBHelper(ctx);
            widgetsmapper = new SWidgetsMapper(dbHelper);
            //Get all widgets and display them on the screen
            //GetAllWidgetsOnScreen();
        }

    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    boolean isLoaded = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ctx = this.getActivity();
        View widgetsView =  inflater.inflate(R.layout.fragment_widgets, container, false);
        widgetmanager = AppWidgetManager.getInstance(ctx);
        widgethost = new LauncherAppWidgetHost(this.getActivity(), R.id.APPWIDGET_HOST_ID);
        widgetsLayout = ((RelativeLayout) widgetsView.findViewById(R.id.widgetsLayout));
//        widgetsLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
//            @Override
//            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
//
//                dbHelper = new SQLiteDBHelper(ctx);
//                widgetsmapper = new SWidgetsMapper(dbHelper);
//                GetAllWidgetsOnScreen();
//                isLoaded = true;
//            }
//        });
        ViewTreeObserver vio = widgetsLayout.getViewTreeObserver();
        vio.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                dbHelper = new SQLiteDBHelper(ctx);
                widgetsmapper = new SWidgetsMapper(dbHelper);
                GetAllWidgetsOnScreen();
                ViewTreeObserver vt = widgetsLayout.getViewTreeObserver();
                vt.removeOnGlobalLayoutListener(this);
            }
        });
        addwidgetBtn = (Button) widgetsView.findViewById(R.id.addwidgetBtn);
        addwidgetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectWidget();
            }
        });
        minflater = inflater;
        return  widgetsView;
    }

    void SelectWidget()
    {
        int appWidgetId = this.widgethost.allocateAppWidgetId();
        Intent pickIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_PICK);
        pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        AddEmptyData(pickIntent);
        startActivityForResult(pickIntent, REQUEST_PICK_WIDGET);
    }


    void AddEmptyData(Intent pickIntent)
    {
        ArrayList customInfo = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_INFO, customInfo);
        ArrayList customextras = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_EXTRAS, customextras);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK)
        {
            if(requestCode == REQUEST_PICK_SHORTCUT)
            {
                ConfigureShortcut(data);
            }
            else if(requestCode == REQUEST_PICK_WIDGET)
            {
                ConfigureWidget(data);
            }
            else if(requestCode == REQUEST_CREATE_APPWIDGET) {
                CreateWidget(data);
            }
        }
        else if(resultCode == Activity.RESULT_CANCELED && data != null)
        {
            int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
            if(appWidgetId != -1)
            {
                widgethost.deleteAppWidgetId(appWidgetId);
            }
        }
    }

    void ConfigureShortcut(Intent data)
    {
        startActivityForResult(data, REQUEST_CREATE_SHORTCUT);
    }

    private void ConfigureWidget(Intent data)
    {
        Bundle extras = data.getExtras();
        int appwidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        AppWidgetProviderInfo widgetProviderInfo = widgetmanager.getAppWidgetInfo(appwidgetId);
        if(widgetProviderInfo.configure != null)
        {
            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            intent.setComponent(widgetProviderInfo.configure);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appwidgetId);
            startActivityForResult(intent, REQUEST_CREATE_APPWIDGET);
        }
        else
        {
            CreateWidget(data);
        }

    }

    int maxLeft;
    int maxTop;

    public void GetAllWidgetsOnScreen()
    {
        widgetsList = new ArrayList<SWidget>();
        widgetsList = widgetsmapper.GetAllWidgets();
        if(widgetsList.size() > 0)
        {
            for(int i =0; i < widgetsList.size(); i++)
            {
                AttachWidget(widgetsList.get(i));
            }
        }
    }

    int ii = 0;

    private void AttachWidget(SWidget widget)
    {
        LinearLayout optionsLayout;
        Button deletewidgetBtn;
        Button movewidgetBtn;

        widgethost.stopListening();
        final AppWidgetProviderInfo widgetProviderInfo = widgetmanager.getAppWidgetInfo(widget.getId());
        Toast.makeText(ctx, widgetProviderInfo.provider.getPackageName(), Toast.LENGTH_LONG).show();
        final int widgetId = widget.getId();
        LauncherAppWidgetHostView hostView = (LauncherAppWidgetHostView) widgethost.createView(this.getActivity(), widget.getId(), widgetProviderInfo);
        hostView.setId(widget.getId());
        hostView.setAppWidget(widget.getId(), widgetProviderInfo);
        hostView.setTag(widget);
        View optionsView =  LayoutInflater.from(ctx).inflate(R.layout.fragment_widget_overlay, (ViewGroup) hostView, true);
        optionsView.setClickable(true);
        optionsLayout = (LinearLayout) optionsView.findViewById(R.id.mergeWidgetLayout);
        deletewidgetBtn = (Button) optionsView.findViewById(R.id.removesenseWidgetBtn);
        movewidgetBtn = (Button) optionsView.findViewById(R.id.relocatesenseWidgetBtn);
        RelativeLayout.LayoutParams l =  new RelativeLayout.LayoutParams(widgetsLayout.getWidth()/3, widgetsLayout.getHeight()/3);
        //RelativeLayout.LayoutParams l =  new RelativeLayout.LayoutParams(200 + 100 * (ii ++), 200 + 100 * (ii ++));
        l.leftMargin = widget.getLeft();
        l.topMargin = widget.getTop();
        optionsView.setLayoutParams(l);
        deletewidgetBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        DeleteWidget(widgetId, (View)v.getParent().getParent());
                    }
                });
        movewidgetBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        View view = (View) v.getParent().getParent();
                        view.setOnTouchListener(new View.OnTouchListener()
                        {
                            int leftmargin;
                            int topmargin;
                            int rootwidth;
                            int rootheight;
                            @Override
                            public boolean onTouch(View v, MotionEvent event)
                            {
                                SWidget widget1 = null;
                                switch(event.getAction())
                                {
                                    case MotionEvent.ACTION_MOVE:
                                        leftmargin=(int)event.getRawX()-v.getWidth()/2;
                                        topmargin=(int)event.getRawY()-v.getHeight()/2;
                                        rootwidth=v.getRootView().getWidth();
                                        rootheight=v.getRootView().getHeight();
                                        RelativeLayout.LayoutParams lp= new RelativeLayout.LayoutParams(v.getWidth(),v.getHeight());
                                        if(leftmargin+v.getWidth()>rootwidth)
                                            leftmargin=rootwidth-v.getWidth();
                                        else if(leftmargin<0)
                                            leftmargin=0;
                                        if(topmargin+v.getHeight()>((View)v.getParent()).getHeight())
                                            topmargin=((View)v.getParent()).getHeight()-v.getHeight();
                                        else if(topmargin<0)
                                            topmargin=0;
                                        lp.leftMargin=leftmargin;
                                        lp.topMargin=topmargin;
                                        v.setLayoutParams(lp);
                                        LauncherAppWidgetHostView hview = ((LauncherAppWidgetHostView) v);
                                        //update new position of the widget
                                        if(hview.getTag() != null)
                                        {
                                            widget1 = ((SWidget) v.getTag());
                                            widget1.setLeft(leftmargin);
                                            widget1.setTop(topmargin);
                                        }
                                        v.setTag(widget1);
                                        break;
                                    case MotionEvent.ACTION_UP:
                                        v.setOnTouchListener(null);
                                        ((LinearLayout)((View)v).findViewById(R.id.mergeWidgetLayout)).setVisibility(View.INVISIBLE);
                                        if(v.getTag() != null) {
                                            widget1 = (SWidget) v.getTag();
                                            widgetsmapper.UpdateWidgets(widget1);
                                        }
                                        widgethost.startListening();
                                        break;
                                }
                                return true;
                            }
                        });
                        //new WidgetAppTouchListener(optionsLayout, widgethost, widget, widgetsmapper));
                    }
                });
        optionsView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = ctx.getPackageManager().getLaunchIntentForPackage(widgetProviderInfo.provider.getPackageName());
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        ctx.startActivity(intent);
                    }
                });
        optionsView.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        widgethost.stopListening();
                        View view = v.findViewById(R.id.mergeWidgetLayout);
                        view.setVisibility(View.VISIBLE);
                        view.bringToFront();
                        //optionsView.requestLayout();
                        //deletewidgetBtn.setVisibility(View.VISIBLE);
                        //movewidgetBtn.setVisibility(View.VISIBLE);
                        return true;
                    }
                });
        widgetsLayout.addView(optionsView, l);
        widgethost.startListening();
    }



    private void CreateWidget(Intent data)
    {
        SWidget newwidget = new SWidget();
        maxLeft = widgetsLayout.getWidth();
        maxTop = widgetsLayout.getHeight();
        Bundle extras = data.getExtras();
        final int appwidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        RelativeLayout.LayoutParams lp =  new RelativeLayout.LayoutParams(widgetsLayout.getWidth()/3, widgetsLayout.getHeight()/3);
        lp.leftMargin = widgetsLayout.getLeft();
        lp.topMargin = widgetsLayout.getTop();
        newwidget.setId(appwidgetId);
        newwidget.setLeft(lp.leftMargin);
        newwidget.setTop(lp.topMargin);
        AddWidgetToDB(newwidget);
        AttachWidget(newwidget);
    }

    private void DeleteWidget(int appWidgetId, View hostView)
    {
        DeleteWidgetFromDB(appWidgetId);
        widgethost.deleteAppWidgetId(appWidgetId);
        widgetsLayout.removeView(hostView);
    }
    //region for widgets DB call
    ArrayList<SWidget> widgetsList;

    private void AddWidgetToDB(SWidget widget)
    {
        widgetsmapper.InsertWidget(widget);
    }

    private void UpdateWidgetToDB(SWidget widget)
    {
        widgetsmapper.UpdateWidgets(widget);
    }

    private void DeleteWidgetFromDB(int appWidgetId)
    {
        widgetsmapper.DeleteWidget(appWidgetId);
    }
}
