//package com.durga.sph.sense.old;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.appwidget.AppWidgetHostView;
//import android.appwidget.AppWidgetManager;
//import android.appwidget.AppWidgetProviderInfo;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.content.pm.ResolveInfo;
//import android.content.res.Resources;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.graphics.PorterDuff;
//import android.graphics.PorterDuffXfermode;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
//import android.graphics.drawable.StateListDrawable;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.preference.PreferenceManager;
//import android.support.v4.app.FragmentActivity;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.GridLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.SlidingDrawer;
//import android.widget.TextView;
//
//import com.durga.sph.sense.AppTouchListener;
//import com.durga.sph.sense.LauncherAppWidgetHost;
//import com.durga.sph.sense.LauncherAppWidgetHostView;
//import com.durga.sph.sense.R;
//import com.durga.sph.sense.SAppAdapter;
//import com.durga.sph.sense.ShortcutClickListener;
//import com.durga.sph.sense.SortApps;
//import com.durga.sph.sense.StoresListing;
//import com.durga.sph.sense.ThemeTools;
//import com.durga.sph.sense.pac;
//
//import java.lang.ref.WeakReference;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
////import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
//
///**
// * Created by durga on 6/26/15.
// */
//public class OldInstalledappsFragment extends FragmentActivity
//{
//    PackageManager packageManager;
//    List<ResolveInfo> pacsList;
//    pac[] pacs;
//
//    //Layout attributes
//    RecyclerView appsView;
//    RelativeLayout homeViewLayout;
//    SlidingDrawer drawer;
//
//    //App widgets
//    AppWidgetManager widgetmanager;
//    LauncherAppWidgetHost widgethost;
//    int REQUEST_CREATE_APPWIDGET = 10;
//    int REQUEST_PICK_WIDGET = 11;
//    int REQUEST_PICK_SHORTCUT = 12;
//    int REQUEST_CREATE_SHORTCUT = 13;
//    int REQUEST_PICK_THEME = 14;
//    int REQUEST_CREATE_THEME = 15;
//    int numwidgets;
//
//    //
//    SharedPreferences prefs;
//    SAppAdapter appAdapterObj;
//    static Activity activity;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_allapps);
//        activity = this;
//        prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        appsView = (RecyclerView) this.findViewById(R.id.recyclerappsView);
//        drawer = (SlidingDrawer) this.findViewById(R.id.drawer);
//        drawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
//            @Override
//            public void onDrawerOpened() {
//                appLauncheable = true;
//            }
//        });
//        homeViewLayout = (RelativeLayout) this.findViewById(R.id.relAppsLayout);
//        packageManager = getPackageManager();
//        widgetmanager = AppWidgetManager.getInstance(this);
//        widgethost = new LauncherAppWidgetHost(this, R.id.APPWIDGET_HOST_ID);
//        //add listener for relative layout
//        homeViewLayout.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v)
//            {
//                AlertDialog.Builder builder = new AlertDialog.Builder(OldInstalledappsFragment.this);
//                String[] items = {getResources().getString(R.string.widget), getResources().getString(R.string.shortcut), getResources().getString(R.string.theme)};
//                builder.setItems(items, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which)
//                    {
//                        switch(which)
//                        {
//                            case 0:
//                                SelectWidget();
//                                break;
//                            case 1:
//                                SelectShortcut();
//                                break;
//                            case 2:
//                                SelectTheme();
//                                break;
//                        }
//
//                    }
//                });
//                builder.create();
//                builder.show();
//                return false;
//            }
//        });
//
//    }
//
//    PacReceiver pacreceiver;
//
//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        //packageManager = getPackageManager();
//        // appsView.addItemDecoration(new MarginDecoration(this));
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,9, GridLayout.HORIZONTAL, false);
//        gridLayoutManager.setSmoothScrollbarEnabled(true);
//        //gridLayoutManager.
//        //LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        appsView.setLayoutManager(gridLayoutManager);
//        appsView.setHasFixedSize(true);
//        gridLayoutManager.setSmoothScrollbarEnabled(true);
//        appsView.setHorizontalScrollBarEnabled(true);
//        appsView.setVerticalScrollBarEnabled(false);
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
//        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
//        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
//        filter.addDataScheme("package");
//        pacreceiver = new PacReceiver();
//        registerReceiver(pacreceiver, filter);
//        //DisplayInstalledAppsList(true);
//        DisplayAppsOnHomeScreen();
//    }
//
//    @Override
//    protected void onPause()
//    {
//        super.onPause();
//        unregisterReceiver(pacreceiver);
//        //   packageManager = null;
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        widgethost.startListening();;
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        widgethost.stopListening();
//    }
//
//    void DisplayAppsOnHomeScreen(){
//        pac app;
//        ResolveInfo info;
//        String name;
//        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
//        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//        pacsList = packageManager.queryIntentActivities(mainIntent, 0);
//        pacs = new pac[pacsList.size()];
//        ArrayList<pac> installedappNamesList = new ArrayList<pac>();
//        StoresListing.storesMap = new HashMap<String, String>();
//        for(int i =0; i < pacsList.size(); i++)
//        {
//            info = pacsList.get(i);
//            app = new pac();
//            if(packageManager != null && info.loadLabel(packageManager) != null)
//            {
//                pacs[i] = new pac();
//                pacs[i].setName(info.activityInfo.name);
//                pacs[i].setPackagename(info.activityInfo.packageName);
//                String text = "Icon width: " + info.loadIcon(packageManager).getIntrinsicWidth();
//                text += "Icon height: " + info.loadIcon(packageManager).getIntrinsicHeight() + "\n";
////                text += "Label width: " + info.loadLabel(packageManager).get;
////                text += "Icon height: " + info.loadLabel(packageManager).getIntrinsicHeight() + "\n";
//
//                pacs[i].setImage(info.loadIcon(packageManager) );
//                pacs[i].setLabel(info.loadLabel(packageManager).toString());
//            }
//        }
//        new SortApps().exchange_sort(pacs);
//        DisplayInstalledAppsList(true);
//    }
//
////    void ShowPagerRecyclerView()
////    {
////        final RecyclerViewPager mRecyclerView = (RecyclerViewPager) this.findViewById(R.id.homeViewPager);
////        LinearLayoutManager layout = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
////        mRecyclerView.setLayoutManager(layout);//setLayoutManager
//////set adapter
////        mRecyclerView.setAdapter(new  LayoutAdapter(activity, mRecyclerView, R.layout.appinfo_row_grid));
////
//////set scroll listener
//////this will show you how to implement a ViewPager like the demo gif
////
////        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
////            @Override
////            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
////                updateState(scrollState);
////            }
////
////            @Override
////            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
////                int childCount = mRecyclerView.getChildCount();
////                int width = mRecyclerView.getChildAt(0).getWidth();
////                int padding  = (mRecyclerView.getWidth() - width)/2;
////               // mCountText.setText("Count: " + childCount);
////
////                for (int j = 0; j < childCount; j++) {
////                    View v = recyclerView.getChildAt(j);
////                    float rate = 0;
////                    if (v.getLeft() <= padding) {
////                        if (v.getLeft() >= padding - v.getWidth()) {
////                            rate = (padding - v.getLeft()) * 1f / v.getWidth();
////                        } else {
////                            rate = 1;
////                        }
////                        v.setScaleY(1 - rate * 0.1f);
////                    } else {
////                        if (v.getLeft() <= recyclerView.getWidth() - padding) {
////                            rate = (recyclerView.getWidth() - padding - v.getLeft()) * 1f / v.getWidth();
////                        }
////                        v.setScaleY(0.9f + rate * 0.1f);
////                    }
////                }
////            }
////        });
////        // registering addOnLayoutChangeListener  aim to setScale at first layout action
////        mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
////            @Override
////            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
////                if(mRecyclerView.getChildCount()<3){
////                    if (mRecyclerView.getChildAt(1) != null) {
////                        View v1 = mRecyclerView.getChildAt(1);
////                        v1.setScaleY(0.9f);
////                    }
////                }else {
////                    if (mRecyclerView.getChildAt(0) != null) {
////                        View v0 = mRecyclerView.getChildAt(0);
////                        v0.setScaleY(0.9f);
////                    }
////                    if (mRecyclerView.getChildAt(2) != null) {
////                        View v2 = mRecyclerView.getChildAt(2);
////                        v2.setScaleY(0.9f);
////                    }
////                }
////
////            }
////        });
////    }
//
////    void GetAppsPerScreenCount()
////    {
//////        int totalHeight = homeViewLayout.getHeight();
//////        int totalwidth = homeViewLayout.getWidth();
//////        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//////        LinearLayout ll = (LinearLayout) li.inflate(R.layout.appinfo_row_grid, null);
//////        ((ImageView)ll.findViewById(R.id.sicon)).setImageBitmap(pacs[0]);
//////        ((TextView)ll.findViewById(R.id.sname)).setText(shortcutLabel);
////        String text = "";
////        for(int i =0;i < pacs.length; i++)
////        {
////
////        }
////    }
//    void SelectTheme()
//    {
//        Intent intent = new Intent(Intent.ACTION_PICK_ACTIVITY);
//        Intent filter = new Intent(Intent.ACTION_MAIN);
//        filter.addCategory("com.anddoes.launcher.THEME");
//        intent.putExtra(Intent.EXTRA_INTENT, filter);
//        startActivityForResult(intent, REQUEST_PICK_THEME);
//    }
//
//    void SelectShortcut()
//    {
//        Intent intent = new Intent(Intent.ACTION_PICK_ACTIVITY);
//        intent.putExtra(Intent.EXTRA_INTENT, new Intent(Intent.ACTION_CREATE_SHORTCUT));
//        startActivityForResult(intent, REQUEST_PICK_SHORTCUT);
//    }
//
//    void SelectWidget()
//    {
//        int appWidgetId = this.widgethost.allocateAppWidgetId();
//        Intent pickIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_PICK);
//        pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
//        AddEmptyData(pickIntent);
//        startActivityForResult(pickIntent, REQUEST_PICK_WIDGET);
//    }
//
//
//    void AddEmptyData(Intent pickIntent)
//    {
//        ArrayList customInfo = new ArrayList();
//        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_INFO, customInfo);
//        ArrayList customextras = new ArrayList();
//        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_EXTRAS, customextras);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(resultCode == RESULT_OK)
//        {
//            if(requestCode == REQUEST_PICK_SHORTCUT)
//            {
//                ConfigureShortcut(data);
//            }
//            else if(requestCode == REQUEST_PICK_WIDGET)
//            {
//                ConfigureWidget(data);
//            }
//            else if(requestCode == REQUEST_CREATE_SHORTCUT)
//            {
//                CreateShortcut(data);
//            }
//            else if(requestCode == REQUEST_CREATE_APPWIDGET) {
//                CreateWidget(data);
//            }
//            else if(requestCode == REQUEST_PICK_THEME)
//            {
//                prefs.edit().putString("theme", data.getComponent().getPackageName()).commit();
//                DisplayInstalledAppsList(false);
//            }
//        }
//        else if(resultCode == RESULT_CANCELED && data != null)
//        {
//            int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
//            if(appWidgetId != -1)
//            {
//                widgethost.deleteAppWidgetId(appWidgetId);
//            }
//        }
//
//    }
//
//    void ConfigureShortcut(Intent data)
//    {
//        startActivityForResult(data, REQUEST_CREATE_SHORTCUT);
//    }
//
//    private void ConfigureWidget(Intent data)
//    {
//        Bundle extras = data.getExtras();
//        int appwidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
//        AppWidgetProviderInfo widgetProviderInfo = widgetmanager.getAppWidgetInfo(appwidgetId);
//        if(widgetProviderInfo.configure != null)
//        {
//            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
//            intent.setComponent(widgetProviderInfo.configure);
//            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appwidgetId);
//            startActivityForResult(intent, REQUEST_CREATE_APPWIDGET);
//        }
//        else
//        {
//            CreateWidget(data);
//        }
//
//    }
//
//    private void CreateShortcut(Intent intent)
//    {
//        Intent.ShortcutIconResource iconResource = intent.getParcelableExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE);
//        Bitmap icon                              = intent.getParcelableExtra(Intent.EXTRA_SHORTCUT_ICON);
//        String shortcutLabel                     = intent.getStringExtra(Intent.EXTRA_SHORTCUT_NAME);
//        Intent shortIntent                       = intent.getParcelableExtra(Intent.EXTRA_SHORTCUT_INTENT);
//
//        if (icon==null){
//            if (iconResource!=null){
//                Resources resources =null;
//                try {
//                    resources = packageManager.getResourcesForApplication(iconResource.packageName);
//                } catch (PackageManager.NameNotFoundException e) {
//                    e.printStackTrace();
//                }
//                if (resources != null) {
//                    int id = resources.getIdentifier(iconResource.resourceName, null, null);
//                    if(resources.getDrawable(id) instanceof StateListDrawable) {
//                        Drawable d = ((StateListDrawable)resources.getDrawable(id)).getCurrent();
//                        icon = ((BitmapDrawable)d).getBitmap();
//                    }else
//                        icon = ((BitmapDrawable)resources.getDrawable(id)).getBitmap();
//                }
//            }
//        }
//
//
//        if (shortcutLabel!=null && shortIntent!=null && icon!=null){
//            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//            lp.leftMargin = 100;
//            lp.topMargin = (int) 100;
//
//            LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            LinearLayout ll = (LinearLayout) li.inflate(R.layout.appinfo_row_grid, null);
//            ((ImageView)ll.findViewById(R.id.sicon)).setImageBitmap(icon);
//            ((TextView)ll.findViewById(R.id.sname)).setText(shortcutLabel);
//
//            ll.setOnLongClickListener(new View.OnLongClickListener() {
//
//                @Override
//                public boolean onLongClick(View v) {
//                    v.setOnTouchListener(new AppTouchListener());
//                    return false;
//                }
//            });
//
//            ll.setOnClickListener(new ShortcutClickListener(this));
//            ll.setTag(shortIntent);
//            homeViewLayout.addView(ll, lp);
//        }
//
//    }
//
//    private void CreateWidget(Intent data)
//    {
//        Bundle extras = data.getExtras();
//        int appwidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
//        AppWidgetProviderInfo widgetProviderInfo = widgetmanager.getAppWidgetInfo(appwidgetId);
//        LauncherAppWidgetHostView hostView = (LauncherAppWidgetHostView) widgethost.createView(this, appwidgetId, widgetProviderInfo);
//        hostView.setAppWidget(appwidgetId, widgetProviderInfo);
//        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(homeViewLayout.getWidth()/3, homeViewLayout.getHeight()/3);
//        lp.leftMargin = numwidgets * (homeViewLayout.getWidth()/3);
//        //lp.topMargin = homeViewLayout.getHeight()/3;
//        hostView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                v.setBackgroundColor(Color.RED);
//                return false;
//            }
//        });
//        homeViewLayout.addView(hostView, lp);
//        drawer.bringToFront();
//        numwidgets ++;
//    }
//
//    private void RemoveWidget(AppWidgetHostView hostView)
//    {
//        homeViewLayout.removeView(hostView);
//    }
//
//
//    private void DisplayInstalledAppsList(boolean init)
//    {
//        pac app;
//        ResolveInfo info;
//        String name;
//        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
//        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//        pacsList = packageManager.queryIntentActivities(mainIntent, 0);
//        pacs = new pac[pacsList.size()];
//        ArrayList<pac> installedappNamesList = new ArrayList<pac>();
//        StoresListing.storesMap = new HashMap<String, String>();
//        for(int i =0; i < pacsList.size(); i++)
//        {
//            info = pacsList.get(i);
//            app = new pac();
//            if(packageManager != null && info.loadLabel(packageManager) != null)
//            {
//                pacs[i] = new pac();
//                pacs[i].setName(info.activityInfo.name);
//                pacs[i].setPackagename(info.activityInfo.packageName);
//                pacs[i].setImage(info.loadIcon(packageManager));
//                pacs[i].setLabel(info.loadLabel(packageManager).toString());
//            }
//        }
//        new SortApps().exchange_sort(pacs);
//        themePacs();
//        //set the recyclerview to display all the installed app names
//        if(init)
//        {
//            appAdapterObj = new SAppAdapter(this, pacs, packageManager, drawer, homeViewLayout);
//            appsView.setAdapter(appAdapterObj);
//        }
//        else
//        {
//            appAdapterObj.appsList = pacs;
//            appAdapterObj.notifyDataSetChanged();
//        }
//
//
//        //SAppPageAdapter pageradapter = new SAppPageAdapter(this);
//
//    }
//
//
//
//    @SuppressWarnings("deprecation")
//    public void themePacs() {
//        //theming vars-----------------------------------------------
//        final int ICONSIZE = ThemeTools.numtodp(65, this);
//        Resources themeRes = null;
//        String resPacName =prefs.getString("theme", "");
//        String iconResource = null;
//        int intres=0;
//        int intresiconback = 0;
//        int intresiconfront = 0;
//        int intresiconmask = 0;
//        float scaleFactor = 1.0f;
//
//        Paint p = new Paint(Paint.FILTER_BITMAP_FLAG);
//        p.setAntiAlias(true);
//
//        Paint origP = new Paint(Paint.FILTER_BITMAP_FLAG);
//        origP.setAntiAlias(true);
//
//        Paint maskp= new Paint(Paint.FILTER_BITMAP_FLAG);
//        maskp.setAntiAlias(true);
//        maskp.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
//
//        if (resPacName.compareTo("")!=0){
//            try{themeRes =packageManager.getResourcesForApplication(resPacName);}catch(Exception e){};
//            if (themeRes!=null){
//                String[] backAndMaskAndFront =ThemeTools.getIconBackAndMaskResourceName(themeRes,resPacName);
//                if (backAndMaskAndFront[0]!=null)
//                    intresiconback=themeRes.getIdentifier(backAndMaskAndFront[0],"drawable",resPacName);
//                if (backAndMaskAndFront[1]!=null)
//                    intresiconmask=themeRes.getIdentifier(backAndMaskAndFront[1],"drawable",resPacName);
//                if (backAndMaskAndFront[2]!=null)
//                    intresiconfront=   themeRes.getIdentifier(backAndMaskAndFront[2],"drawable",resPacName);
//            }
//        }
//
//        BitmapFactory.Options uniformOptions = new BitmapFactory.Options();
//        uniformOptions.inScaled=false;
//        uniformOptions.inDither=false;
//        uniformOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
//
//        Canvas origCanv;
//        Canvas canvas;
//        scaleFactor=ThemeTools.getScaleFactor(themeRes,resPacName);
//        Bitmap back=null;
//        Bitmap mask=null;
//        Bitmap front=null;
//        Bitmap scaledBitmap = null;
//        Bitmap scaledOrig = null;
//        Bitmap orig = null;
//
//        if (resPacName.compareTo("")!=0 && themeRes!=null){
//            try{
//                if (intresiconback!=0)
//                    back =BitmapFactory.decodeResource(themeRes,intresiconback,uniformOptions);
//            }catch(Exception e){}
//            try{
//                if (intresiconmask!=0)
//                    mask = BitmapFactory.decodeResource(themeRes,intresiconmask,uniformOptions);
//            }catch(Exception e){}
//            try{
//                if (intresiconfront!=0)
//                    front = BitmapFactory.decodeResource(themeRes,intresiconfront,uniformOptions);
//            }catch(Exception e){}
//        }
//        //theming vars-----------------------------------------------
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = false;
//        options.inPreferredConfig = Bitmap.Config.RGB_565;
//        options.inDither = true;
//
//        for(int I=0;I<pacs.length;I++) {
//            if (themeRes!=null){
//                iconResource=null;
//                intres=0;
//                iconResource=ThemeTools.getResourceName(themeRes, resPacName, "ComponentInfo{"+pacs[I].packagename+"/"+pacs[I].name+"}");
//                if (iconResource!=null){
//                    intres = themeRes.getIdentifier(iconResource,"drawable",resPacName);
//                }
//
//                if (intres!=0){//has single drawable for app
//                    pacs[I].image = new BitmapDrawable(BitmapFactory.decodeResource(themeRes,intres,uniformOptions));
//                }else{
//                    orig=Bitmap.createBitmap(pacs[I].image.getIntrinsicWidth(), pacs[I].image.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//                    pacs[I].image.setBounds(0, 0, pacs[I].image.getIntrinsicWidth(), pacs[I].image.getIntrinsicHeight());
//                    pacs[I].image.draw(new Canvas(orig));
//
//                    scaledOrig =Bitmap.createBitmap(ICONSIZE, ICONSIZE, Bitmap.Config.ARGB_8888);
//                    scaledBitmap = Bitmap.createBitmap(ICONSIZE, ICONSIZE, Bitmap.Config.ARGB_8888);
//                    canvas = new Canvas(scaledBitmap);
//                    if (back!=null){
//                        canvas.drawBitmap(back, ThemeTools.getResizedMatrix(back, ICONSIZE, ICONSIZE), p);
//                    }
//
//                    origCanv=new Canvas(scaledOrig);
//                    orig=ThemeTools.getResizedBitmap(orig, ((int)(ICONSIZE*scaleFactor)), ((int)(ICONSIZE*scaleFactor)));
//                    origCanv.drawBitmap(orig, scaledOrig.getWidth()-(orig.getWidth()/2)-scaledOrig.getWidth()/2 ,scaledOrig.getWidth()-(orig.getWidth()/2)-scaledOrig.getWidth()/2, origP);
//
//                    if (mask!=null){
//                        origCanv.drawBitmap(mask,ThemeTools.getResizedMatrix(mask, ICONSIZE, ICONSIZE), maskp);
//                    }
//
//                    if (back!=null){
//                        canvas.drawBitmap(ThemeTools.getResizedBitmap(scaledOrig,ICONSIZE,ICONSIZE), 0, 0,p);
//                    }else
//                        canvas.drawBitmap(ThemeTools.getResizedBitmap(scaledOrig,ICONSIZE,ICONSIZE), 0, 0,p);
//
//                    if (front!=null)
//                        canvas.drawBitmap(front,ThemeTools.getResizedMatrix(front, ICONSIZE, ICONSIZE), p);
//
//                    pacs[I].image = new BitmapDrawable(scaledBitmap);
//                }
//            }
//        }
//
//
//        front=null;
//        back=null;
//        mask=null;
//        scaledOrig=null;
//        orig=null;
//        scaledBitmap=null;
//        canvas=null;
//        origCanv=null;
//        p=null;
//        maskp=null;
//        resPacName=null;
//        iconResource=null;
//        intres=0;
//    }
//
//    public static boolean appLauncheable = true;
//
//    public final AsyncHandler AsyncHandlerObj = new AsyncHandler(this);
//    static final class AsyncHandler extends Handler
//    {
//
//        private final WeakReference<OldInstalledappsFragment> mActivity;
//
//        public AsyncHandler(OldInstalledappsFragment apps)
//        {
//            mActivity = new WeakReference<OldInstalledappsFragment>(apps);
//        }
//
//        @Override
//        public void handleMessage(Message msg)
//        {
//            super.handleMessage(msg);
//            switch(msg.what)
//            {
//                case 1:
//                {
//                    //display all app names
//                    if(msg.obj != null) {
//                        // mActivity.get().DisplayInstalledAppsList((List<ResolveInfo>) msg.obj);
//                    }
//                }
//                break;
//
//            }
//        }
//    };
//
//    public class PacReceiver extends BroadcastReceiver
//    {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            DisplayInstalledAppsList(false);
//        }
//    }
//}
//
