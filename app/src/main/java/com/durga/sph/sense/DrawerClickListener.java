package com.durga.sph.sense;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by durga on 6/29/15.
 */
public class DrawerClickListener implements AdapterView.OnItemClickListener {

    Context ctx;
    pac[] pacforAdapter;
    PackageManager pmforListener;
    public DrawerClickListener(Context context, pac[] pacs, PackageManager pm)
    {
        ctx = context;
        pacforAdapter = pacs;
        pmforListener = pm;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if(InstalledAppsFragment.appLauncheable) {
            Intent intent = pmforListener.getLaunchIntentForPackage(pacforAdapter[position].getName());
            ctx.startActivity(intent);
        }

    }
}
