//package com.durga.sph.sense;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.speech.RecognizerIntent;
//import android.support.v4.app.FragmentActivity;
//import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.view.ViewTreeObserver;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.trnql.smart.activity.ActivityEntry;
//import com.trnql.smart.base.SmartCompatActivity;
//import com.trnql.smart.location.AddressEntry;
//
//
//public class MainActivity extends SmartCompatActivity implements URLConstants {
//    private static final float  BITMAP_SCALE        = 0.6f; //smaller is blurrier
//    private static final float  BLUR_RADIUS         = 25f;
//    private static final String COLOUR_FILTER       = "#FFAAAAAA";
//    private static final int    SPEECH_REQUEST_CODE = 1234;
//
//    //EditText et_inputText;
////    TextView                tv_weatherRec;
////    TextView tv_sun;
//   // Toolbar toolbar;
////    ImageView               img_weather;
////    ImageView               img_input;
////    //BackgroundTarget        target;
////    ImageView backdrop;
////    //FloatingActionButton    fab;
//    //CollapsingToolbarLayout collapsingToolbar;
//    String                  currentLocality;
//    //int backdropWidth, backdropHeight;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        //et_inputText = (EditText) findViewById(R.id.et_inputText);
//        //setSupportActionBar(toolbar);
//
//        // load from db
//        currentLocality = getDBManager().getDB_KVP(R.id.db_kvp_locality).get("locality");
//
//        // get imageview width & height
////        ViewTreeObserver vto = backdrop.getViewTreeObserver();
////        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
////            public boolean onPreDraw() {
////                backdrop.getViewTreeObserver().remllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloveOnPreDrawListener(this);
////                backdropWidth = backdrop.getMeasuredWidth();
////                backdropHeight = backdrop.getMeasuredHeight();
////       //         updateBackdrop();
////                return true;
////            }
////        });
//
//
//    }
//    @Override
//    protected void smartAddressChange(AddressEntry address)
//    {
//
//        String locality = address.getLocality();
//
//        if (currentLocality == null || !currentLocality.equals(locality)) {
//            currentLocality = locality;
//            getDBManager().getDB_KVP(R.id.db_kvp_locality).add("locality", locality);
//
// //           updateBackdrop();
//           // collapsingToolbar.setTitle(getString(R.string.title_default) + currentLocality);
//        }
//
//    }
//    public void fabClicked(View view) {
//   //     updateBackdrop();
//    }
//    @Override
//    protected void smartOnViewsInflated() {
//     //   updateBackdrop();
//    }
//
////    private void updateBackdrop() {
////
////        if (backdropWidth > 0 && backdropHeight > 0 && currentLocality != null) {
////            if (urlconstants.containsKey(currentLocality)) {
////                Picasso.with(getApplicationContext()).load(urlconstants.get(currentLocality))
////                        .resize(backdropWidth, backdropHeight)
////                        .centerCrop()
////                        .into(target);
////            }
////        }
////    }
//
////    @Override
////    protected void smartActivityChange(ActivityEntry userActivity) {
////        if (userActivity.isInVehicle()) {
////            et_inputText.setFocusableInTouchMode(false);
////            et_inputText.setHint(getString(R.string.input_text_default) + "Speech");
////            img_input.setImageResource(R.drawable.voice);
////        } else {
////            et_inputText.setFocusableInTouchMode(true);
////            et_inputText.setHint(getString(R.string.input_text_default) + "Keyboard");
////            img_input.setImageResource(R.drawable.keyboard);
////        }
////        et_inputText.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
////                if (v.isFocusableInTouchMode()) {
////                    imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
////                } else {
////                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
////                    startVoiceRecognition();
////                }
////            }
////        });
////    }
//
//    private void startVoiceRecognition() {
//        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speech Input");
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, SPEECH_REQUEST_CODE);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
//        startActivityForResult(intent, SPEECH_REQUEST_CODE);
//    }
//
//
////    @Override
////    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
////        super.onActivityResult(requestCode, resultCode, data);
////        if (resultCode == Activity.RESULT_OK) {
////            if (requestCode == SPEECH_REQUEST_CODE) {
////                et_inputText.setText(data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
////            }
////        }
////    }
//}