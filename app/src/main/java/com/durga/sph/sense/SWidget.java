package com.durga.sph.sense;

/**
 * Created by durga on 7/15/15.
 */
public class SWidget
{
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    int id;
    int left;
    int top;
    int width;
    int height;
//    int minWidth;
//    int minHeight;
//    int maxWidth;
//    int maxHeight;

    public SWidget()
    {

    }

    public SWidget(int sid, int sleft, int stop, int sheight, int swidth)
    {
        id = sid;
        left = sleft;
        top = stop;
        height = sheight;
        width = swidth;
    }
}
