package com.durga.sph.sense;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

/**
 * Created by durga on 7/9/15.
 */
public class LauncherActivity extends FragmentActivity
{
    private MainPagerAdapter mainPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher_pager);
        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(mainPagerAdapter);
        viewPager.setOffscreenPageLimit(1);
        //viewPager.setCurrentItem(1);
        //viewPager.getAdapter().notifyDataSetChanged();
    }
}
