package com.durga.sph.sense;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by durga on 6/26/15.
 */
public class pac implements Serializable
{
    transient Drawable image;
    String name;
    String packagename;
    String label;
    String iconlocation;
    boolean landscape;

    public int getX() {
        return X;
    }

    public void setX(int x) {
        X = x;
    }

    public int getY() {
        return Y;
    }

    public void setY(int y) {
        Y = y;
    }

    public String getIconlocation() {
        return iconlocation;
    }

    public void setIconlocation(String iconlocation) {
        this.iconlocation = iconlocation;
    }

    int X;
    int Y;

    public pac()
    {

    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Drawable getImage()
    {
        return image;
    }

    public void setImage(Drawable image)
    {
        this.image = image;
    }

    public void CacheIcon(){
        String location = InstalledAppsFragment.activity.getApplicationInfo().dataDir + "/cachedApps";
        if(iconlocation == null)
        {
            new File(location).mkdirs();
            if(image != null)
            {
                FileOutputStream fos = null;
                iconlocation =location + packagename + name;
                try
                {
                    fos = new FileOutputStream(iconlocation);

                }
                catch(FileNotFoundException ex){
                    ex.printStackTrace() ;
                }
                if(fos != null)
                {
                    ThemeTools.drawableToBitmap(image).compress(Bitmap.CompressFormat.PNG,100,fos);
                    try
                    {
                        fos.flush() ;
                        fos.close() ;
                    }
                    catch(IOException ex)
                    {
                        ex.printStackTrace();
                    }
                }
                else
                {
                    iconlocation = null;
                }
            }



        }
    }

    public Bitmap getCachedIcon(){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inDither = true;
        if(iconlocation != null)
        {
            File cachedicon = new File(iconlocation);
            if(cachedicon.exists()){
               return  BitmapFactory.decodeFile(cachedicon.getAbsolutePath(), options);
            }
        }
        return null;
    }

    public void DeleteIcon(){
        if(iconlocation != null)
        {
            new File(iconlocation).delete();
        }
    }
}
