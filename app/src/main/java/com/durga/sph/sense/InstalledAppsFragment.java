package com.durga.sph.sense;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

//import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

/**
 * Created by durga on 6/26/15.
 */
public class InstalledAppsFragment extends GetBaseAppsListFragment
{
    //SharedPreferences prefs;
    SAppAdapter appAdapterObj;
    static Activity activity;
    static boolean appLauncheable;
    PacReceiver pacreceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        View allAppsView =  inflater.inflate(R.layout.fragment_allapps, container, false);
        activity = this.getActivity();
        //prefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        appsView = (RecyclerView) allAppsView.findViewById(R.id.allappsView);
        packageManager = this.getActivity().getPackageManager();
        return  allAppsView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addDataScheme("package");
        pacreceiver = new PacReceiver();
        activity.registerReceiver(pacreceiver, filter);
        DisplayInstalledAppsOnScreen();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        activity.unregisterReceiver(pacreceiver);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    void DisplayInstalledAppsOnScreen()
    {
        appLauncheable = true;
        DisplayInstalledAppsList(true);
    }

//    @SuppressWarnings("deprecation")
//    public void themePacs() {
//        //theming vars-----------------------------------------------
//        final int ICONSIZE = ThemeTools.numtodp(65, this);
//        Resources themeRes = null;
//        String resPacName =prefs.getString("theme", "");
//        String iconResource = null;
//        int intres=0;
//        int intresiconback = 0;
//        int intresiconfront = 0;
//        int intresiconmask = 0;
//        float scaleFactor = 1.0f;
//
//        Paint p = new Paint(Paint.FILTER_BITMAP_FLAG);
//        p.setAntiAlias(true);
//
//        Paint origP = new Paint(Paint.FILTER_BITMAP_FLAG);
//        origP.setAntiAlias(true);
//
//        Paint maskp= new Paint(Paint.FILTER_BITMAP_FLAG);
//        maskp.setAntiAlias(true);
//        maskp.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
//
//        if (resPacName.compareTo("")!=0){
//            try{themeRes =packageManager.getResourcesForApplication(resPacName);}catch(Exception e){};
//            if (themeRes!=null){
//                String[] backAndMaskAndFront =ThemeTools.getIconBackAndMaskResourceName(themeRes,resPacName);
//                if (backAndMaskAndFront[0]!=null)
//                    intresiconback=themeRes.getIdentifier(backAndMaskAndFront[0],"drawable",resPacName);
//                if (backAndMaskAndFront[1]!=null)
//                    intresiconmask=themeRes.getIdentifier(backAndMaskAndFront[1],"drawable",resPacName);
//                if (backAndMaskAndFront[2]!=null)
//                    intresiconfront=   themeRes.getIdentifier(backAndMaskAndFront[2],"drawable",resPacName);
//            }
//        }
//
//        BitmapFactory.Options uniformOptions = new BitmapFactory.Options();
//        uniformOptions.inScaled=false;
//        uniformOptions.inDither=false;
//        uniformOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
//
//        Canvas origCanv;
//        Canvas canvas;
//        scaleFactor=ThemeTools.getScaleFactor(themeRes,resPacName);
//        Bitmap back=null;
//        Bitmap mask=null;
//        Bitmap front=null;
//        Bitmap scaledBitmap = null;
//        Bitmap scaledOrig = null;
//        Bitmap orig = null;
//
//        if (resPacName.compareTo("")!=0 && themeRes!=null){
//            try{
//                if (intresiconback!=0)
//                    back =BitmapFactory.decodeResource(themeRes,intresiconback,uniformOptions);
//            }catch(Exception e){}
//            try{
//                if (intresiconmask!=0)
//                    mask = BitmapFactory.decodeResource(themeRes,intresiconmask,uniformOptions);
//            }catch(Exception e){}
//            try{
//                if (intresiconfront!=0)
//                    front = BitmapFactory.decodeResource(themeRes,intresiconfront,uniformOptions);
//            }catch(Exception e){}
//        }
//        //theming vars-----------------------------------------------
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = false;
//        options.inPreferredConfig = Bitmap.Config.RGB_565;
//        options.inDither = true;
//
//        for(int I=0;I<pacs.length;I++) {
//            if (themeRes!=null){
//                iconResource=null;
//                intres=0;
//                iconResource=ThemeTools.getResourceName(themeRes, resPacName, "ComponentInfo{"+pacs[I].packagename+"/"+pacs[I].name+"}");
//                if (iconResource!=null){
//                    intres = themeRes.getIdentifier(iconResource,"drawable",resPacName);
//                }
//
//                if (intres!=0){//has single drawable for app
//                    pacs[I].image = new BitmapDrawable(BitmapFactory.decodeResource(themeRes,intres,uniformOptions));
//                }else{
//                    orig=Bitmap.createBitmap(pacs[I].image.getIntrinsicWidth(), pacs[I].image.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//                    pacs[I].image.setBounds(0, 0, pacs[I].image.getIntrinsicWidth(), pacs[I].image.getIntrinsicHeight());
//                    pacs[I].image.draw(new Canvas(orig));
//
//                    scaledOrig =Bitmap.createBitmap(ICONSIZE, ICONSIZE, Bitmap.Config.ARGB_8888);
//                    scaledBitmap = Bitmap.createBitmap(ICONSIZE, ICONSIZE, Bitmap.Config.ARGB_8888);
//                    canvas = new Canvas(scaledBitmap);
//                    if (back!=null){
//                        canvas.drawBitmap(back, ThemeTools.getResizedMatrix(back, ICONSIZE, ICONSIZE), p);
//                    }
//
//                    origCanv=new Canvas(scaledOrig);
//                    orig=ThemeTools.getResizedBitmap(orig, ((int)(ICONSIZE*scaleFactor)), ((int)(ICONSIZE*scaleFactor)));
//                    origCanv.drawBitmap(orig, scaledOrig.getWidth()-(orig.getWidth()/2)-scaledOrig.getWidth()/2 ,scaledOrig.getWidth()-(orig.getWidth()/2)-scaledOrig.getWidth()/2, origP);
//
//                    if (mask!=null){
//                        origCanv.drawBitmap(mask,ThemeTools.getResizedMatrix(mask, ICONSIZE, ICONSIZE), maskp);
//                    }
//
//                    if (back!=null){
//                        canvas.drawBitmap(ThemeTools.getResizedBitmap(scaledOrig,ICONSIZE,ICONSIZE), 0, 0,p);
//                    }else
//                        canvas.drawBitmap(ThemeTools.getResizedBitmap(scaledOrig,ICONSIZE,ICONSIZE), 0, 0,p);
//
//                    if (front!=null)
//                        canvas.drawBitmap(front,ThemeTools.getResizedMatrix(front, ICONSIZE, ICONSIZE), p);
//
//                    pacs[I].image = new BitmapDrawable(scaledBitmap);
//                }
//            }
//        }
//
//
//        front=null;
//        back=null;
//        mask=null;
//        scaledOrig=null;
//        orig=null;
//        scaledBitmap=null;
//        canvas=null;
//        origCanv=null;
//        p=null;
//        maskp=null;
//        resPacName=null;
//        iconResource=null;
//        intres=0;
//    }
//
//    public static boolean appLauncheable = true;
//
//    public final AsyncHandler AsyncHandlerObj = new AsyncHandler(this);
//    static final class AsyncHandler extends Handler
//    {
//
//        private final WeakReference<InstalledAppsFragment> mActivity;
//
//        public AsyncHandler(InstalledAppsFragment apps)
//        {
//            mActivity = new WeakReference<InstalledAppsFragment>(apps);
//        }
//
//        @Override
//        public void handleMessage(Message msg)
//        {
//            super.handleMessage(msg);
//            switch(msg.what)
//            {
//                case 1:
//                {
//                    //display all app names
//                    if(msg.obj != null) {
//                        // mActivity.get().DisplayInstalledAppsList((List<ResolveInfo>) msg.obj);
//                    }
//                }
//                break;
//
//            }
//        }
//    };

    public class PacReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent) {
            DisplayInstalledAppsList(false);
        }
    }
}

