package com.durga.sph.sense;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;


/**
 * Created by durga on 7/9/15.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {
    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0)
        {
            return new WidgetsFragment();
        }
        else if (i == 1){
            return new TopAppsFragment();
        }
        else
        {
            return new InstalledAppsFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int i) {
        if (i == 0) {
            return "Widgets";
        } else if (i == 1){
            return "Top Apps";
        } else {
            return "All Apps";
        }
    }
}

