package com.durga.sph.sense;

import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by durga on 6/29/15.
 */
public class AppTouchListener implements View.OnTouchListener {

    int leftmargin;
    int topmargin;
    int rootwidth;
    int rootheight;

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
     switch(event.getAction())
     {
         case MotionEvent.ACTION_MOVE:
             leftmargin = (int) event.getRawX() - v.getWidth()/2;
             topmargin = (int) event.getRawY() - v.getHeight()/2;
             rootwidth = v.getRootView().getWidth();
             rootheight = v.getRootView().getHeight();
             RelativeLayout.LayoutParams lp = new  RelativeLayout.LayoutParams(v.getWidth(), v.getHeight());
             if(leftmargin + v.getWidth() > rootwidth)
                 leftmargin = rootwidth - v.getWidth();
             else if(leftmargin < 0)
                 leftmargin = 0;
             if(topmargin + v.getHeight() > ((View)v.getParent()).getHeight())
                 topmargin = ((View)v.getParent()).getHeight() - v.getHeight();
             else if(topmargin < 0)
                 topmargin = 0;

             lp.leftMargin = leftmargin;
             lp.topMargin =  topmargin;
             v.setLayoutParams(lp);
             break;
         case MotionEvent.ACTION_UP:
             v.setOnTouchListener(null);
             break;
     }
        return true;
    }
}
