package com.durga.sph.sense;

import android.content.Context;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by durga on 7/1/15.
 */
public class SerializationTools {

    public static void SerializableData(AppSerializableData obj)
    {
        FileOutputStream fos;
        try
        {
            fos = InstalledAppsFragment.activity.openFileOutput("data", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fos);
            objectOutputStream.writeObject(obj);
            objectOutputStream.close();

        }catch(FileNotFoundException ex){}
            catch(IOException ex){}


    }

    public static AppSerializableData LoadSerializableData()
    {
        ObjectInputStream is = null;
        try
        {
            is = new ObjectInputStream(InstalledAppsFragment.activity.openFileInput("data"));
            Object obj =is.readObject();
            if(obj instanceof AppSerializableData)
            {
                return (AppSerializableData)obj;
            }
        }
        catch(EOFException ex){}
        catch(FileNotFoundException ex){}
        catch(ClassNotFoundException ex){}
        catch(IOException ex){}
        try
        {
            if(is != null)
            {
                is.close();
            }
        }
        catch (IOException ex)
        {

        }
        return null;
    }
}
