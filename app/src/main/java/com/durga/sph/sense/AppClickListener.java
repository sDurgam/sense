package com.durga.sph.sense;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.View;

/**
 * Created by durga on 6/29/15.
 */
public class AppClickListener implements View.OnClickListener
{
    Context ctx;
    String[] attrs;
    public AppClickListener(Context context, String[] data)
    {
        ctx = context;
        attrs = data;
    }

    @Override
    public void onClick(View u)
    {
        Intent launchIntent = new Intent(Intent.ACTION_MAIN);
        launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        ComponentName cp = new ComponentName(attrs[1], attrs[0]);
        launchIntent.setComponent(cp);
        ctx.startActivity(launchIntent);
    }
}
