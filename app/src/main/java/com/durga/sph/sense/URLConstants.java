package com.durga.sph.sense;

/**
 * Created by durga on 6/17/15.
 */
import java.util.HashMap;

public interface URLConstants {
    HashMap<String, String> urlconstants = new HashMap<String, String>() {{
        put("Berkeley", "https://upload.wikimedia.org/wikipedia/commons/7/74/ChezPanisse.jpg");
        put("Cupertino", "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Seagate_Technology_Headquarters_Cupertino.jpg/1280px-Seagate_Technology_Headquarters_Cupertino.jpg");
        put("Palo Alto", "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Palo_Alto_Circle_%28University_%26_Alma%29-102_University_Ave--Designed_by_Joseph_Bellomo_Architects_2014-05-19_16-38.jpg/1280px-Palo_Alto_Circle_%28University_%26_Alma%29-102_University_Ave--Designed_by_Joseph_Bellomo_Architects_2014-05-19_16-38.jpg");
        put("Fremont", "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Mission_Peak_over_Lake_Elizabeth%2C_in_Fremont%2C_California.JPG/1280px-Mission_Peak_over_Lake_Elizabeth%2C_in_Fremont%2C_California.JPG");
        put("San Jose", "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/South_San_Jose_%28crop%29.jpg/1280px-South_San_Jose_%28crop%29.jpg");
        put("Los Altos", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Apple_Garage.jpg/1280px-Apple_Garage.jpg");
        put("Menlo Park", "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Facebook_Headquarters_1_Hacker_Way_Menlo_Park.jpg/1280px-Facebook_Headquarters_1_Hacker_Way_Menlo_Park.jpg");
        put("Milpitas", "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Milpitas_view2.JPG/1280px-Milpitas_view2.JPG");
        put("Mountain View", "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Google_Campus.jpg/1280px-Google_Campus.jpg");
        put("Redwood City", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Redwood_Shores_February_2013_001.jpg/1280px-Redwood_Shores_February_2013_001.jpg");
        put("San Francisco", "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/San_Francisco_downtown_late_afternoon_looking_southeast.JPG/1280px-San_Francisco_downtown_late_afternoon_looking_southeast.JPG");
        put("Santa Clara", "http://upload.wikimedia.org/wikipedia/commons/9/92/Santa_Clara_city_public_swimming_pool_kids_practice.jpg");
        put("Sunnyvale", "http://upload.wikimedia.org/wikipedia/commons/6/60/Murphystreetsunnyvale.jpg");
        put("Woodside", "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Woodside_Store%2C_471_Kings_Mountain_Rd.%2C_Woodside%2C_CA_9-18-2011_5-30-50_PM.JPG/1280px-Woodside_Store%2C_471_Kings_Mountain_Rd.%2C_Woodside%2C_CA_9-18-2011_5-30-50_PM.JPG");
    }};
}//end interface URLConstants
