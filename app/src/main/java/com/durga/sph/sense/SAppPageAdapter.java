package com.durga.sph.sense;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by durga on 7/6/15.
 */
public class SAppPageAdapter extends PagerAdapter
{
    List<SAppAdapter.AppViewHolder> pacs;

    public SAppPageAdapter(List<SAppAdapter.AppViewHolder> pacsList)
    {
        this.pacs = pacsList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        SAppAdapter.AppViewHolder view = pacs.get(position);
        container.addView(view.itemView);
        return view.itemView;
    }

    @Override
    public int getCount()
    {
        return pacs.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
