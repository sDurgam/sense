package com.durga.sph.sense;

/**
 * Created by durga on 6/30/15.
 */
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class ShortcutClickListener implements OnClickListener {
    Context mContext;

    public ShortcutClickListener(Context ctxt){
        mContext = ctxt;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent data;
        data= (Intent) v.getTag();
        mContext.startActivity(data);

    }

}